import { StatusBar } from "expo-status-bar";
import React from "react";
import NavigatorScreen from "./navigation";
import { Provider } from "react-redux";
import reduxStore from "./redux/Store";
import { PersistGate } from "redux-persist/integration/react";

export default function App() {
  const { store, persistedStore } = reduxStore();
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistedStore}>
        <NavigatorScreen />
        <StatusBar style="auto" />
      </PersistGate>
    </Provider>
  );
}
