import firebase from "firebase";
//web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAgUmH5XLTuVAEyO8EM7kDLTuxpwv5HOF0",
  authDomain: "saylove-e1880.firebaseapp.com",
  projectId: "saylove-e1880",
  storageBucket: "saylove-e1880.appspot.com",
  messagingSenderId: "733828662964",
  appId: "1:733828662964:web:55389947718eb24f8fb9e1",
};
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
export default firebase;
