import React, { useState, useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { NavigationContainer } from "@react-navigation/native";
import { useSelector } from "react-redux";
import {
  Home,
  Login,
  Register,
  Notifications,
  Profile,
  SayLove,
  LoveCalculator,
} from "../screens";

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

export default function NavigatorScreen() {
  const loggedEmail = useSelector((state) => state.auth.dataUser);

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        {loggedEmail ? null : <Stack.Screen name="Login" component={Login} />}
        {!loggedEmail ? null : <Stack.Screen name="Home" component={MainApp} />}
        {!loggedEmail ? null : (
          <Stack.Screen name="SayLove" component={SayLove} />
        )}
        {!loggedEmail ? null : (
          <Stack.Screen name="LoveCalculator" component={LoveCalculator} />
        )}
        {!loggedEmail ? null : (
          <Stack.Screen name="Notifications" component={Notifications} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const MainApp = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="white"
      inactiveColor="#FF9E9E"
      barStyle={{
        backgroundColor: "#564AA5",
        alignItems: "center",
        paddingVertical: 6,
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: "Home",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Notifications"
        component={Notifications}
        options={{
          tabBarLabel: "Notifications",
          tabBarColor: "blue",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="bell" color={color} size={26} />
          ),
          tabBarBadge: 5,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: "Profile",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};
