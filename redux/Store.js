import { createStore } from "redux";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Reducer from "./Reducer";
import { persistReducer, persistStore } from "redux-persist";

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
};

export default () => {
  const persistedReducer = persistReducer(persistConfig, Reducer);
  const store = createStore(persistedReducer);
  const persistedStore = persistStore(store);
  return { store, persistedStore };
};
