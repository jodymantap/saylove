import { combineReducers } from "redux";

const initialState = {
  isLogin: false,
  profile: false,
  notify: false,
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case "login": {
      return {
        ...state,
        isLogin: true,
        dataUser: action.payload,
      };
    }
    case "profile": {
      return {
        ...state,
        profile: true,
        profileData: action.payload,
      };
    }
    case "notify": {
        return {
          ...state,
          notify: true,
          notifData: action.payload,
        };
      }
    default:
      return state;
  }
};

export default combineReducers({ auth });
