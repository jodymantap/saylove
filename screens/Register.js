import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default function Register() {
    return(
        <View style={styles.container}>
            <Text>This is Register</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})
