import React, { useState, useEffect } from "react";
import AppLoading from "expo-app-loading";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { useSelector } from "react-redux";
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import { useDispatch } from "react-redux";

import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  ActivityIndicator,
  ToastAndroid,
} from "react-native";
import {
  useFonts,
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
} from "@expo-google-fonts/poppins";

const statusBarHeight = StatusBar.currentHeight;
const window = Dimensions.get("window");
const ratio = (window.width * 80) / 100 / 749; //749 is actual image width

export default function Profile({ navigation }) {
  const [showDate, setShowDate] = useState(false);
  const email = useSelector((state) => state.auth.dataUser);
  const [nameInput, setNameInput] = useState("");
  const [birthdateInput, setBirthdateInput] = useState(new Date());
  const profileData = useSelector((state) => state.auth.profileData);
  const [loaded, setLoaded] = useState(true);
  const dispatch = useDispatch();

  let [fontsLoaded, error] = useFonts({
    popThin: Poppins_100Thin,
    popThinItalic: Poppins_100Thin_Italic,
    popExtraLight: Poppins_200ExtraLight,
    popExtraLightItalic: Poppins_200ExtraLight_Italic,
    popRegular: Poppins_400Regular,
    popRegularItalic: Poppins_400Regular_Italic,
    popSemibold: Poppins_600SemiBold,
    popSemiboldItalic: Poppins_600SemiBold_Italic,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  const submitProfile = () => {
    setLoaded(false);
    const data = {
      fullname: nameInput,
      birthdate: moment(birthdateInput).format("LL"),
    };
    console.log(data);
    dispatch({ type: "profile", payload: data });
    setLoaded(true);
    ToastAndroid.showWithGravity(
      "Your profile is updated!",
      ToastAndroid.SHORT,
      ToastAndroid.CENTER
    );
  };

  const logout = () => {
    dispatch({ type: "login", payload: undefined });
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          style={{
            width: (window.width * 80) / 100 / 3,
            height: (ratio * 296) / 3,
            alignItems: "center",
            justifyContent: "center",
          }}
          source={require("../assets/images/logo-white.png")}
        />
      </View>
      <ScrollView style={{ width: window.width }}>
        <View style={styles.contentContainer}>
          <View style={styles.headlineContainer}>
            <Text style={styles.headline}>
              {profileData ? profileData.fullname : "No Name"}
            </Text>
          </View>

          <View>
            <Text style={styles.cardTitle}>Date of birth</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              alignItems: "center",
              marginBottom: 20,
            }}
          >
            <Text style={styles.cardText}>
              {profileData ? profileData.birthdate : "No Birthdate"}
            </Text>
            <MaterialCommunityIcons
              style={{ marginLeft: 10 }}
              name="cake"
              color="#564AA5"
              size={26}
            />
          </View>

          <View>
            <Text style={styles.cardTitle}>Email</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              alignItems: "center",
              marginBottom: 20,
            }}
          >
            <Text style={styles.cardText}>{email ? email : "No data"}</Text>
            <MaterialCommunityIcons
              style={{ marginLeft: 10 }}
              name="email"
              color="#564AA5"
              size={26}
            />
          </View>
          <TextInput
            style={styles.formInput}
            placeholder="Enter your full name"
            placeholderTextColor="#564AA5"
            value={nameInput}
            onChangeText={(value) => setNameInput(value)}
          />
          <TouchableOpacity
            onPress={() => setShowDate(true)}
            style={styles.formInput}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Text style={{ color: "#564AA5" }}>
                {"Enter your birthdate: " + moment(birthdateInput).format("LL")}
              </Text>
              <MaterialCommunityIcons
                style={{ marginLeft: 10 }}
                name="calendar"
                color="#564AA5"
                size={26}
              />
            </View>
          </TouchableOpacity>
          {showDate ? (
            <DateTimePicker
              value={birthdateInput}
              mode="date"
              display="default"
              onChange={(e, date) => {
                if (date) {
                  setBirthdateInput(date);
                  setShowDate(false);
                }
              }}
            />
          ) : null}
          <TouchableOpacity
            onPress={() => submitProfile()}
            activeOpacity={0.8}
            style={styles.submitButton}
          >
            <Text style={styles.submitButtonText}>UPDATE</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => logout()}
            activeOpacity={0.8}
            style={styles.logoutButton}
          >
            <Text style={styles.logoutButtonText}>LOGOUT</Text>
          </TouchableOpacity>
          {loaded ? null : <ActivityIndicator size="large" color="#564AA5" />}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: statusBarHeight,
  },
  headerContainer: {
    backgroundColor: "#564AA5",
    width: window.width,
    paddingVertical: 15,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  contentContainer: {
    flex: 1,
    alignItems: "center",
    marginTop: 20,
  },
  cardContainer: {
    backgroundColor: "#564AA5",
    borderRadius: 10,
    paddingVertical: 20,
    paddingHorizontal: 10,
    marginBottom: 8,
    marginTop: 8,
    width: (window.width * 80) / 100,
  },
  cardTitle: {
    color: "#564AA5",
    fontFamily: "popSemibold",
  },
  cardText: {
    color: "#564AA5",
    fontFamily: "popRegularItalic",
    lineHeight: 20,
    marginLeft: 10,
  },
  headline: {
    color: "#564AA5",
    fontFamily: "popSemibold",
    fontSize: 20,
    paddingBottom: 20,
  },
  headlineContainer: {
    backgroundColor: "#FF9E9E",
    height: 16,
    marginBottom: 40,
    paddingHorizontal: 10,
  },
  detailButton: {
    backgroundColor: "#564AA5",
    paddingHorizontal: 8,
    paddingVertical: 3,
    borderRadius: 10,
    marginBottom: 30,
  },
  detailButtonText: {
    color: "white",
  },
  formInput: {
    width: "80%",
    paddingHorizontal: 10,
    paddingVertical: 8,
    marginBottom: 10,
    borderColor: "#564AA5",
    borderRadius: 10,
    borderWidth: 1,
    color: "#564AA5",
    fontFamily: "popRegular",
  },
  submitButton: {
    marginTop: 10,
    alignItems: "center",
    backgroundColor: "#564AA5",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#564AA5",
    width: "80%",
    paddingVertical: 10,
  },
  submitButtonText: {
    color: "white",
    fontFamily: "popSemibold",
    fontSize: 20,
  },
  logoutButton: {
    marginTop: 10,
    alignItems: "center",
    backgroundColor: "white",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#564AA5",
    width: "80%",
    paddingVertical: 10,
  },
  logoutButtonText: {
    color: "#564AA5",
    fontFamily: "popSemibold",
    fontSize: 20,
  },
});
