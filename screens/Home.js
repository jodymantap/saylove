import React, { useState, useEffect } from "react";
import AppLoading from "expo-app-loading";
import firebase from "../assets/api/firebase";
import { useSelector, useDispatch } from "react-redux";

import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import {
  useFonts,
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
} from "@expo-google-fonts/poppins";

const statusBarHeight = StatusBar.currentHeight;
const window = Dimensions.get("window");
const ratio = (window.width * 80) / 100 / 749; //749 is actual image width

export default function Home({ navigation }) {
  const [letter, setLetter] = useState("");
  const dispatch = useDispatch();
  const profileData = useSelector((state) => state.auth.profileData);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    let loveRef = firebase.database().ref();
    // loveRef.orderByKey().on("child_added", (snapshot) => {
    //   let arr = Object.keys(snapshot);
    //   let arrObj = arr.map((key) => {
    //     return { ...snapshot[key], id: key };
    //   });
    //   // const filteredData = allData.find((item) => item.loverName === "Jody Mantap");
    //   console.log(arrObj);
    // });
    loveRef
      .child("saylove")
      .get()
      .then((snapshot) => {
        if (snapshot.exists()) {
          let arr = Object.keys(snapshot.val());
          let arrObj = arr.map((key) => {
            return snapshot.val()[key];
          });
          const filteredData = arrObj.filter(
            (item) =>
              item.loverName === profileData.fullname &&
              item.date === profileData.birthdate
          );
          console.log(filteredData);
          setLetter(filteredData[filteredData.length - 1].message);
          dispatch({ type: "notify", payload: filteredData });
        } else {
          console.log("No Data");
        }
      })
      .catch((err) => console.log(err));
  };

  let [fontsLoaded, error] = useFonts({
    popThin: Poppins_100Thin,
    popThinItalic: Poppins_100Thin_Italic,
    popExtraLight: Poppins_200ExtraLight,
    popExtraLightItalic: Poppins_200ExtraLight_Italic,
    popRegular: Poppins_400Regular,
    popRegularItalic: Poppins_400Regular_Italic,
    popSemibold: Poppins_600SemiBold,
    popSemiboldItalic: Poppins_600SemiBold_Italic,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          style={{
            width: (window.width * 80) / 100 / 3,
            height: (ratio * 296) / 3,
            alignItems: "center",
            justifyContent: "center",
          }}
          source={require("../assets/images/logo-white.png")}
        />
      </View>
      <ScrollView style={{ width: window.width }}>
        <View style={styles.contentContainer}>
          <View style={styles.headlineContainer}>
            <Text style={styles.headline}>Love letters you received</Text>
          </View>
          {letter !== "" ? (
            <TouchableOpacity activeOpacity={0.8} style={styles.cardContainer}>
              <Text style={styles.cardText}>" {letter} "</Text>
            </TouchableOpacity>
          ) : (
            <Text style={{ color: "#564AA5", marginBottom: 10 }}>No data</Text>
          )}
          <TouchableOpacity
            onPress={() => getData()}
            activeOpacity={0.8}
            style={styles.detailButton}
          >
            <Text style={styles.detailButtonText}>Refresh</Text>
          </TouchableOpacity>

          <View style={styles.headlineContainer}>
            <Text style={styles.headline}>Some cool stuffs</Text>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate("SayLove")}
            activeOpacity={0.8}
            style={styles.cardContainer}
          >
            <Text style={styles.cardTitle}>Say Love</Text>
            <Text style={styles.cardText}>
              We encourage you to say your love to your lover, anonymously!
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate("LoveCalculator")}
            activeOpacity={0.8}
            style={styles.cardContainer}
          >
            <Text style={styles.cardTitle}>Love Calculator</Text>
            <Text style={styles.cardText}>
              Calculating the compatibility of you and your lover for fun!
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: statusBarHeight,
  },
  headerContainer: {
    backgroundColor: "#564AA5",
    width: window.width,
    paddingVertical: 15,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  contentContainer: {
    flex: 1,
    alignItems: "center",
    marginTop: 20,
  },
  cardContainer: {
    backgroundColor: "#564AA5",
    borderRadius: 10,
    paddingVertical: 20,
    paddingHorizontal: 10,
    marginBottom: 8,
    marginTop: 8,
    width: (window.width * 80) / 100,
  },
  cardTitle: {
    color: "white",
    fontFamily: "popSemibold",
  },
  cardText: {
    color: "white",
    fontFamily: "popRegularItalic",
    lineHeight: 20,
  },
  headline: {
    color: "#564AA5",
    fontFamily: "popSemibold",
    fontSize: 20,
    paddingBottom: 20,
  },
  headlineContainer: {
    backgroundColor: "#FF9E9E",
    height: 16,
    marginBottom: 20,
    paddingHorizontal: 10,
  },
  detailButton: {
    backgroundColor: "#564AA5",
    paddingHorizontal: 8,
    paddingVertical: 3,
    borderRadius: 10,
    marginBottom: 30,
  },
  detailButtonText: {
    color: "white",
  },
});
