import React, { useState, useEffect } from "react";
import AppLoading from "expo-app-loading";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
  StatusBar,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import firebase from "../assets/api/firebase";
import {
  useFonts,
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
} from "@expo-google-fonts/poppins";
const statusBarHeight = StatusBar.currentHeight;
const window = Dimensions.get("window");
const ratio = (window.width * 80) / 100 / 749; //749 is actual image width

export default function Notifications() {
  const profileData = useSelector((state) => state.auth.profileData);
  const [letters, setLetters] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    let loveRef = firebase.database().ref("saylove");
    loveRef.on("value", (snapshot) => {
      let arr = Object.keys(snapshot.val());
      let arrObj = arr.map((key) => {
        return snapshot.val()[key];
      });
      const filteredData = arrObj.filter(
        (item) =>
          item.loverName === profileData.fullname &&
          item.date === profileData.birthdate
      );
      console.log(filteredData);
      setLetters(filteredData);
    });
  };

  let [fontsLoaded, error] = useFonts({
    popThin: Poppins_100Thin,
    popThinItalic: Poppins_100Thin_Italic,
    popExtraLight: Poppins_200ExtraLight,
    popExtraLightItalic: Poppins_200ExtraLight_Italic,
    popRegular: Poppins_400Regular,
    popRegularItalic: Poppins_400Regular_Italic,
    popSemibold: Poppins_600SemiBold,
    popSemiboldItalic: Poppins_600SemiBold_Italic,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }
  return (
    <>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Image
            style={{
              width: (window.width * 80) / 100 / 3,
              height: (ratio * 296) / 3,
              alignItems: "center",
              justifyContent: "center",
            }}
            source={require("../assets/images/logo-white.png")}
          />
        </View>
        <ScrollView style={{ width: window.width }}>
          {letters.map((e, i) => {
            return (
              <View key={i} style={styles.contentContainer}>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={styles.cardContainer}
                >
                  <Text style={styles.cardText}>" {e.message} "</Text>
                </TouchableOpacity>
              </View>
            );
          })}
        </ScrollView>
      </View>
      <View style={styles.shadowContainer}>
        {!letters.length > 0 ? (
          <View style={{ flex: 1, alignItems: "center" }}>
            <Text style={{ color: "#564AA5" }}>
              You have no letters received
            </Text>
            <TouchableOpacity
              onPress={() => getData()}
              style={styles.detailButton}
            >
              <Text style={styles.detailButtonText}>Refresh</Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: statusBarHeight,
  },
  shadowContainer: {
    flex: 1,
    alignItems: "center",
  },
  headerContainer: {
    backgroundColor: "#564AA5",
    width: window.width,
    paddingVertical: 15,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  cardContainer: {
    backgroundColor: "#564AA5",
    borderRadius: 10,
    paddingVertical: 20,
    paddingHorizontal: 10,
    marginBottom: 8,
    marginTop: 8,
    width: (window.width * 80) / 100,
  },
  cardText: {
    color: "white",
    fontFamily: "popRegularItalic",
    lineHeight: 20,
  },
  contentContainer: {
    flex: 1,
    alignItems: "center",
    marginTop: 20,
  },
  detailButton: {
    backgroundColor: "#564AA5",
    paddingHorizontal: 8,
    paddingVertical: 3,
    borderRadius: 10,
    marginBottom: 30,
  },
  detailButtonText: {
    color: "white",
  },
});
