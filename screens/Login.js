import React, { useState, useEffect, useRef } from "react";
import * as firebase from "firebase";
import AppLoading from "expo-app-loading";
import { useSelector } from "react-redux";

import {
  useFonts,
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
} from "@expo-google-fonts/poppins";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Keyboard,
  Animated,
  Dimensions,
  ActivityIndicator,
} from "react-native";
import { useDispatch } from "react-redux";

export default function Login({ navigation }) {
  const [keyboardStatus, setKeyboardStatus] = useState(false);
  const window = Dimensions.get("window");
  const ratio = (window.width * 80) / 100 / 749; //749 is actual image width
  const _keyboardDidShow = () => fadeOut();
  const _keyboardDidHide = () => fadeIn();
  const fadeAnimation = useRef(new Animated.Value(100)).current;
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoaded, setIsLoaded] = useState(true);
  const [errorNich, setError] = useState(false);
  const dispatch = useDispatch();
  const loggedEmail = useSelector((state) => state.auth.dataUser);

  //web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyAgUmH5XLTuVAEyO8EM7kDLTuxpwv5HOF0",
    authDomain: "saylove-e1880.firebaseapp.com",
    projectId: "saylove-e1880",
    storageBucket: "saylove-e1880.appspot.com",
    messagingSenderId: "733828662964",
    appId: "1:733828662964:web:55389947718eb24f8fb9e1",
  };

  //initialize firebase
  useEffect(() => {
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
  }, []);

  let [fontsLoaded, error] = useFonts({
    popThin: Poppins_100Thin,
    popThinItalic: Poppins_100Thin_Italic,
    popExtraLight: Poppins_200ExtraLight,
    popExtraLightItalic: Poppins_200ExtraLight_Italic,
    popRegular: Poppins_400Regular,
    popRegularItalic: Poppins_400Regular_Italic,
    popSemibold: Poppins_600SemiBold,
    popSemiboldItalic: Poppins_600SemiBold_Italic,
  });

  const fadeIn = () => {
    Animated.timing(fadeAnimation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  };

  const fadeOut = () => {
    Animated.timing(fadeAnimation, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  const submitLogin = async () => {
    setIsLoaded(false);
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((resp) => {
        console.log(resp.user.email);
        dispatch({ type: "login", payload: resp.user.email });
        setIsLoaded(true);
        navigation.navigate("Home");
      })
      .catch((err) => {
        console.log(err);
        setIsLoaded(true);
        setError(true);
      });
  };

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
    };
  }, []);

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <View style={styles.container}>
      <Animated.Image
        style={{
          marginBottom: 50,
          opacity: fadeAnimation,
          width: (window.width * 80) / 100,
          height: ratio * 296,
        }}
        source={require("../assets/images/logo.png")}
      />
      <Text
        style={{
          marginBottom: 10,
          fontFamily: "popRegular",
          color: "#564AA5",
        }}
      >
        Hi, There !
      </Text>
      <TextInput
        style={styles.formInput}
        placeholder="Enter your email"
        placeholderTextColor="#564AA5"
        value={email}
        onChangeText={(value) => setEmail(value)}
      />
      <TextInput
        secureTextEntry={true}
        style={styles.formInput}
        placeholder="Enter your password"
        placeholderTextColor="#564AA5"
        value={password}
        onChangeText={(value) => setPassword(value)}
      />
      <TouchableOpacity
        onPress={() => submitLogin()}
        activeOpacity={0.8}
        style={styles.formButton}
      >
        <Text style={styles.buttonText}>LOGIN</Text>
      </TouchableOpacity>
      {isLoaded ? null : <ActivityIndicator size="large" color="#564AA5" />}
      {errorNich ? (
        <Text
          style={{
            fontFamily: "popRegularItalic",
            color: "#564AA5",
          }}
        >
          Email or Password is incorect
        </Text>
      ) : null}
      <Text style={{ fontFamily: "popRegular", marginTop: 10 }}>
        New lovers?{" "}
        <Text
          style={{
            fontFamily: "popRegularItalic",
            textDecorationLine: "underline",
            color: "#564AA5",
          }}
        >
          Sign up
        </Text>
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  formInput: {
    width: "80%",
    paddingLeft: 10,
    paddingVertical: 8,
    marginBottom: 10,
    borderColor: "#564AA5",
    borderRadius: 10,
    borderWidth: 1,
    color: "#564AA5",
    fontFamily: "popRegular",
  },
  formButton: {
    marginTop: 10,
    alignItems: "center",
    backgroundColor: "#564AA5",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#564AA5",
    width: "80%",
    paddingVertical: 10,
  },
  buttonText: {
    color: "white",
    fontFamily: "popSemibold",
    fontSize: 20,
  },
});
