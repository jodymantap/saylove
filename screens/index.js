import Home from "./Home";
import Login from "./Login";
import Register from "./Register";
import Notifications from "./Notifications";
import Profile from "./Profile";
import SayLove from "./menus/SayLove";
import LoveCalculator from "./menus/LoveCalculator";

export {
  Home,
  Login,
  Register,
  Notifications,
  Profile,
  SayLove,
  LoveCalculator,
};
