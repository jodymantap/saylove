import React, { useState, useEffect } from "react";
import AppLoading from "expo-app-loading";
import DateTimePicker from "@react-native-community/datetimepicker";
import { useSelector } from "react-redux";
import firebase from "../../assets/api/firebase";
import moment from "moment";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  ActivityIndicator,
  ToastAndroid,
} from "react-native";
import {
  useFonts,
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
} from "@expo-google-fonts/poppins";

const statusBarHeight = StatusBar.currentHeight;
const window = Dimensions.get("window");
const ratio = (window.width * 80) / 100 / 749; //749 is actual image width

export default function SayLove({ navigation }) {
  const [userName, setUserName] = useState("Andreas");
  const [loverName, setLoverName] = useState("");
  const [date, setDate] = useState(new Date());
  const [showDate, setShowDate] = useState(false);
  const [message, setMessage] = useState("");
  const profileData = useSelector((state) => state.auth.profileData);
  const [loaded, setLoaded] = useState(true);
  let [fontsLoaded, error] = useFonts({
    popThin: Poppins_100Thin,
    popThinItalic: Poppins_100Thin_Italic,
    popExtraLight: Poppins_200ExtraLight,
    popExtraLightItalic: Poppins_200ExtraLight_Italic,
    popRegular: Poppins_400Regular,
    popRegularItalic: Poppins_400Regular_Italic,
    popSemibold: Poppins_600SemiBold,
    popSemiboldItalic: Poppins_600SemiBold_Italic,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  const submitLetter = () => {
    setLoaded(false);
    const loveRef = firebase.database().ref("saylove");
    const data = {
      loverName,
      date: moment(date).format("LL"),
      message,
    };
    loveRef
      .push(data)
      .then((resp) => {
        console.log(resp);
        // alert()
        setLoaded(true);
        navigation.navigate("Home");
        ToastAndroid.showWithGravity(
          "Your love letter is sent!",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
      })
      .catch((err) => console.log(err));
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          style={{
            width: (window.width * 80) / 100 / 3,
            height: (ratio * 296) / 3,
            alignItems: "center",
            justifyContent: "center",
          }}
          source={require("../../assets/images/logo-white.png")}
        />
      </View>
      <ScrollView style={{ width: window.width }}>
        <View style={styles.contentContainer}>
          <View style={styles.headlineContainer}>
            <Text style={styles.headline}>Welcome to Say Love</Text>
          </View>

          <TouchableOpacity activeOpacity={0.8} style={styles.cardContainer}>
            <Text style={styles.cardText}>
              " Hi {profileData.fullname}, Your are going to fill some specific data of someone
              you love in this form. If that person is also on this app, we are
              going to send it to them. Your name will not be seen by them. "
            </Text>
          </TouchableOpacity>

          <TextInput
            style={styles.formInput}
            placeholder="Enter your lover's full name"
            placeholderTextColor="#564AA5"
            value={loverName}
            onChangeText={(value) => setLoverName(value)}
          />
          <TouchableOpacity
            onPress={() => setShowDate(true)}
            style={styles.formInput}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Text style={{ color: "#564AA5" }}>
                His/Her Birthdate: {moment(date).format("LL")}
              </Text>
              <MaterialCommunityIcons
                style={{ marginLeft: 10 }}
                name="calendar"
                color="#564AA5"
                size={26}
              />
            </View>
          </TouchableOpacity>
          {showDate ? (
            <DateTimePicker
              value={date}
              mode="default"
              display="default"
              onChange={(e, date) => {
                if (date) {
                  setDate(date);
                  setShowDate(false);
                }
              }}
            />
          ) : null}
          <TextInput
            style={styles.formInput}
            placeholderTextColor="#564AA5"
            multiline={true}
            placeholder="Enter your love message"
            numberOfLines={4}
            onChangeText={(text) => setMessage(text)}
            value={message}
          />

          <TouchableOpacity
            onPress={() => submitLetter()}
            activeOpacity={0.8}
            style={styles.submitButton}
          >
            <Text style={styles.submitButtonText}>SEND LOVE</Text>
          </TouchableOpacity>
          {loaded ? null : <ActivityIndicator size="large" color="#564AA5" />}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: statusBarHeight,
  },
  headerContainer: {
    backgroundColor: "#564AA5",
    width: window.width,
    paddingVertical: 15,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  contentContainer: {
    flex: 1,
    alignItems: "center",
    marginTop: 20,
  },
  cardContainer: {
    backgroundColor: "#564AA5",
    borderRadius: 10,
    paddingVertical: 20,
    paddingHorizontal: 10,
    marginBottom: 20,
    marginTop: 8,
    width: (window.width * 80) / 100,
  },
  cardTitle: {
    color: "white",
    fontFamily: "popSemibold",
  },
  cardText: {
    color: "white",
    fontFamily: "popRegularItalic",
    lineHeight: 20,
  },
  headline: {
    color: "#564AA5",
    fontFamily: "popSemibold",
    fontSize: 20,
    paddingBottom: 20,
  },
  headlineContainer: {
    backgroundColor: "#FF9E9E",
    height: 16,
    marginBottom: 20,
    paddingHorizontal: 10,
  },
  submitButton: {
    marginTop: 10,
    alignItems: "center",
    backgroundColor: "#564AA5",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#564AA5",
    width: "80%",
    paddingVertical: 10,
  },
  submitButtonText: {
    color: "white",
    fontFamily: "popSemibold",
    fontSize: 20,
  },
  formInput: {
    width: "80%",
    paddingHorizontal: 10,
    paddingVertical: 8,
    marginBottom: 10,
    borderColor: "#564AA5",
    borderRadius: 10,
    borderWidth: 1,
    color: "#564AA5",
    fontFamily: "popRegular",
  },
});
